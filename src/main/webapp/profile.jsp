<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Page personnelle</title>
</head>
<body>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <h1>Votre page personnelle: </h1>
        ${utilisateur.prenom}
        ${utilisateur.nom}
    </div>
    <div class="col-md-offset-3 col-md-6">
        Vos donn�es<br/>
        Mail: ${utilisateur.email}<br/>
        Adresse: ${utilisateur.ville.nomVille}
    </div>
    <div class="col-md-offset-5 col-md-2">
        <a href="/LogoutServlet" class="btn btn-large btn-info">LOGOUT</a>
    </div>
</div>


</body>
</html>

</body>
</html>
