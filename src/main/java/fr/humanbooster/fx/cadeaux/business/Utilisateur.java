package fr.humanbooster.fx.cadeaux.business;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
public class Utilisateur {

	@Id
	// crée une colonne auto-incrementée lors de la génération de la classe
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUtilisateur;

	@Column(length = 100)
	private String nom;
	@Column(length = 100)
	private String prenom;
	@Email
	@Column(unique = true)
	private String email;
	@Size(min=5, message = "Ce champs doit contenir 5 caractères minimum")
	private String pwd;
	private Date dateInscription;

	@OneToMany(mappedBy = "utilisateur", fetch=FetchType.EAGER) //
	// ATTENTION ajouter mappedBy dès qu'on utilise @OneToMany
	// signifie: dans la classe commande, il y a un attribut article auquel on fait appel
	// l'utilisateur peut avoir de ONE à MANY commandes
	private List<Commande> commandes;

	@ManyToOne
	// il y a plusieurs utilisateurs dans une ville
	// on associe une ville à un seul utilisteur
	private Ville ville;

	public Utilisateur () {}


	public Utilisateur(String nom, String prenom, String email, String pwd, Date dateInscription, List<Commande> commandes, Ville ville) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.pwd = pwd;
		this.dateInscription = dateInscription;
		this.commandes = commandes;
		this.ville = ville;
	}

	public Utilisateur(String nom, String prenom, String email, String pwd, Ville ville) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.pwd = pwd;
		this.dateInscription = dateInscription;
		this.ville = ville;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "Utilisateur{" +
				"idUtilisateur=" + idUtilisateur +
				", nom='" + nom + '\'' +
				", prenom='" + prenom + '\'' +
				", email='" + email + '\'' +
				", pwd='" + pwd + '\'' +
				", dateInscription=" + dateInscription +
				", ville=" + ville +
				'}';
	}
}
