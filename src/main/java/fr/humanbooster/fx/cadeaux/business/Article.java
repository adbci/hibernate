package fr.humanbooster.fx.cadeaux.business;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Article {

	@Id
	// crée une colonne auto-incrementée lors de la génération de la classe
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idArticle;
	@Size(min=5)
	private String designation;
	private int nbPoints;
	private int stock;

	// Large Object - longues descriptions
	@Lob
	private String description;

	// un article appartient à une catégorie (qui elle contient donc plusieus articles)
	@ManyToOne
	private Categorie categorie;

	@OneToMany(mappedBy = "article", fetch=FetchType.EAGER)
	private List<Commande> commandes;

	public Article(String designation, int nbPoints, int stock, String description, Categorie categorie, List<Commande> commandes) {
		this.designation = designation;
		this.nbPoints = nbPoints;
		this.stock = stock;
		this.description = description;
		this.categorie = categorie;
		this.commandes = commandes;
	}

	public Article() {
		super();
	}

	@Override
	public String toString() {
		return "Article{" +
				"idArticle=" + idArticle +
				", designation='" + designation + '\'' +
				", nbPoints=" + nbPoints +
				", stock=" + stock +
				", description='" + description + '\'' +
				", categorie=" + categorie +
				", commandes=" + commandes +
				'}';
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public int getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(int idArticle) {
		this.idArticle = idArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getNbPoints() {
		return nbPoints;
	}

	public void setNbPoints(int nbPoints) {
		this.nbPoints = nbPoints;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

}
