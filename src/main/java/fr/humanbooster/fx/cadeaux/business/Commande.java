package fr.humanbooster.fx.cadeaux.business;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class Commande {

	@Id
	// crée une colonne auto-incrementée lors de la génération de la classe
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCommande;

	@ManyToOne
	// une commande appartient à un seul utilisateur
	private Utilisateur utilisateur;

	@ManyToOne
	// une commande contient un seul article
	private Article article;

	private Date dateCommande;

	public Commande() {
		super();
	}

	public Commande(Utilisateur utilisateur, Article article) {
		this.utilisateur = utilisateur;
		this.article = article;
	}

	public int getIdCommande() {
		return idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	@Override
	public String toString() {
		return "Commande{" +
				"idCommande=" + idCommande +
				", dateCommande=" + dateCommande +
				'}';
	}
}