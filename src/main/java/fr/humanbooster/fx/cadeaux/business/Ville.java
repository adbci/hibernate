package fr.humanbooster.fx.cadeaux.business;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Ville {

	@Id
	// grace à @Id HIbernate sait que la clé primaire est idVille
	// crée une colonne auto-incrementée lors de la génération de la classe
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idVille;
	private String nomVille;

	@OneToMany(mappedBy = "ville", fetch=FetchType.EAGER)
	// on associe un seul utilisateur à une ville
	// il y a plusieurs utilisateurs dans une ville
	private List<Utilisateur> utilisateurs;

	public Ville () {}

	public Ville(String nomVille, List<Utilisateur> utilisateurs) {
		this.nomVille = nomVille;
		this.utilisateurs = utilisateurs;
	}

	public int getIdVille() {
		return idVille;
	}

	public void setIdVille(int idVille) {
		this.idVille = idVille;
	}

	public String getNomVille() {
		return nomVille;
	}

	public void setNomVille(String nomVille) {
		this.nomVille = nomVille;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	@Override
	public String toString() {
		return "Ville{" +
				"idVille=" + idVille +
				", nomVille='" + nomVille + '\'' +
				'}';
	}
}