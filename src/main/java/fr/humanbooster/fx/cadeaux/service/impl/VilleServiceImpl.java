package fr.humanbooster.fx.cadeaux.service.impl;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;
import fr.humanbooster.fx.cadeaux.dao.VilleDao;
import fr.humanbooster.fx.cadeaux.dao.impl.ArticleDaoImpl;
import fr.humanbooster.fx.cadeaux.dao.impl.VilleDaoImpl;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.VilleService;

import java.util.List;

public class VilleServiceImpl implements VilleService {

	private VilleDao villeDao = new VilleDaoImpl();
	
	public List<Ville> recupereVilles() {
		villeDao.openCurrentSessionWithTransaction();
		List<Ville> villes = villeDao.findAll();
		System.out.println("Nombre de villes en base: " + villes.size());
		villeDao.closeCurrentSessionwithTransaction();
		return villes;
	}

	public Ville recupereVilleById(int idVille) {
		villeDao.openCurrentSessionWithTransaction();
		Ville ville = villeDao.findById(idVille);
		villeDao.closeCurrentSessionwithTransaction();
		return ville;
	}

}
