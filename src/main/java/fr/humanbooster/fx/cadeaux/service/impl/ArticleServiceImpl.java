package fr.humanbooster.fx.cadeaux.service.impl;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;
import fr.humanbooster.fx.cadeaux.dao.impl.ArticleDaoImpl;
import fr.humanbooster.fx.cadeaux.service.ArticleService;

public class ArticleServiceImpl implements ArticleService {

	private ArticleDao articleDao = new ArticleDaoImpl();
	
	public List<Article> recupereArticles() {
		articleDao.openCurrentSessionWithTransaction();
		List<Article> articles = articleDao.findAll();
		System.out.println("Nombre d'articles en base: " + articles.size());
		articleDao.closeCurrentSessionwithTransaction();
		return articles;
	}


	public Article recupereArticleById(int idArticle) {
		articleDao.openCurrentSessionWithTransaction();
		Article article = articleDao.findById(idArticle);
		articleDao.closeCurrentSessionwithTransaction();
		return article;
	}

}
