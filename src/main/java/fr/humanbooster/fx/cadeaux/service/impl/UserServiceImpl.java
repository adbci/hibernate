package fr.humanbooster.fx.cadeaux.service.impl;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.dao.UserDao;
import fr.humanbooster.fx.cadeaux.dao.VilleDao;
import fr.humanbooster.fx.cadeaux.dao.impl.UserDaoImpl;
import fr.humanbooster.fx.cadeaux.dao.impl.VilleDaoImpl;
import fr.humanbooster.fx.cadeaux.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao = new UserDaoImpl();
    private VilleDao villeDao = new VilleDaoImpl();

    public List<Utilisateur> recupereUtilisateurs() {
        userDao.openCurrentSessionWithTransaction();
        List<Utilisateur> utilisateurs = userDao.findAll();
        System.out.println("Nombre d'utilisateurs en base: " + utilisateurs.size());
        userDao.closeCurrentSessionwithTransaction();
        return utilisateurs;
    }

    public Utilisateur recupereUtilisateurById(int idUtilisateur){
        userDao.openCurrentSessionWithTransaction();
        Utilisateur utilisateur = userDao.findById(idUtilisateur);
        userDao.closeCurrentSessionwithTransaction();
        return utilisateur;
    }
    public Utilisateur createUser(String nom, String prenom, String email, String pwd, Ville ville) {
        Utilisateur userToAdd = new Utilisateur(nom, prenom, email, pwd, ville);
        userDao.openCurrentSessionWithTransaction();
        userDao.create(userToAdd);
        System.out.println("Utilisateur créé avec succès");
        userDao.closeCurrentSessionwithTransaction();
        return userToAdd;
    }

    public Utilisateur loginUser(String email, String pwd) {
        Utilisateur utilisateur = null;
        userDao.openCurrentSessionWithTransaction();
        utilisateur = userDao.loginUser(email, pwd);

        if(utilisateur!=null) {
            System.out.println("Utilisateur identifié avec succès");
            userDao.closeCurrentSessionwithTransaction();
            return utilisateur;
        }
        return utilisateur;
    }

}
