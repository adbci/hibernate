package fr.humanbooster.fx.cadeaux.service;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;

import java.util.List;

public interface UserService {

	public List<Utilisateur> recupereUtilisateurs();

	public Utilisateur recupereUtilisateurById(int idUtilisateur);


	public Utilisateur createUser(String nom, String prenom, String email, String pwd, Ville ville);

	public Utilisateur loginUser(String email, String pwd);



}
