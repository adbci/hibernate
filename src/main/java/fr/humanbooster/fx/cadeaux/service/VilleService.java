package fr.humanbooster.fx.cadeaux.service;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Ville;

import java.util.List;

public interface VilleService {

	public List<Ville> recupereVilles();

	public Ville recupereVilleById(int idVille);

}
