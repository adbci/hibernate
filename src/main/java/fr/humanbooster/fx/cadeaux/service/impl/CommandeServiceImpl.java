package fr.humanbooster.fx.cadeaux.service.impl;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;
import fr.humanbooster.fx.cadeaux.dao.CommandeDao;
import fr.humanbooster.fx.cadeaux.dao.impl.ArticleDaoImpl;
import fr.humanbooster.fx.cadeaux.dao.impl.CommandeDaoImpl;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CommandeService;

import java.util.Date;
import java.util.List;

public class CommandeServiceImpl implements CommandeService {

	private CommandeDao commandeDao = new CommandeDaoImpl();
	
	public List<Commande> recupererCommandes() {
		commandeDao.openCurrentSessionWithTransaction();
		List<Commande> commandes = commandeDao.findAll();
		System.out.println("Nombre de commandes en base: " + commandes.size());
		commandeDao.closeCurrentSessionwithTransaction();
		return commandes;
	}

	public Commande creerCommande(Utilisateur utilisateur, Article article){

		Commande commandeToAdd = new Commande(utilisateur, article);
		commandeDao.openCurrentSessionWithTransaction();
		commandeDao.creerCommande(utilisateur, article);
		System.out.println("Commande créé avec succès");
		commandeDao.closeCurrentSessionwithTransaction();
		return commandeToAdd;

	}

	public boolean effacerCommande(int idCommande){
		return false;
	}
	public Commande recupererCommandeById(int idCommande){
		return null;
	}
}
