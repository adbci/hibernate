package fr.humanbooster.fx.cadeaux.service;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;

import java.util.Date;
import java.util.List;

public interface CommandeService {

	public List<Commande> recupererCommandes();
	public boolean effacerCommande(int idCommande);
	public Commande creerCommande(Utilisateur utilisateur, Article article);
	public Commande recupererCommandeById(int idCommande);

}
