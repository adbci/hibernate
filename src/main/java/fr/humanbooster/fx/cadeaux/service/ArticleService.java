package fr.humanbooster.fx.cadeaux.service;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Article;

public interface ArticleService {

	public List<Article> recupereArticles();
	public Article recupereArticleById(int idArticle);

}
