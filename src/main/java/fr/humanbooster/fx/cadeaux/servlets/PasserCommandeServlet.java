package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CommandeService;
import fr.humanbooster.fx.cadeaux.service.UserService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.CommandeServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Servlet implementation class ArticlesServlet
 */
public class PasserCommandeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private CommandeService cs = new CommandeServiceImpl();
    private ArticleService as = new ArticleServiceImpl();
    private UserService userService = new UserServiceImpl();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasserCommandeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idArticle = request.getParameter("id");
        System.out.println(idArticle);

        String idUtilisateur = request.getSession().getAttribute("idUtilisateur").toString();
        System.out.println(idUtilisateur);

        // Article
        int idArticleAchete = Integer.parseInt(idArticle);
        // on cherche l'objet article dans la base depuis l'id fourni par le formulaire
        Article articleSaisi = as.recupereArticleById(idArticleAchete);

        System.out.println("Article acheté :" + articleSaisi.toString());

        // Utilisateur
        int idUtilisateurEnCours = Integer.parseInt(idUtilisateur);
        // on cherche l'objet article dans la base depuis l'id fourni par le formulaire
        Utilisateur utilisateurSaisi = userService.recupereUtilisateurById(idUtilisateurEnCours);

        System.out.println("ID utilisateur :" + utilisateurSaisi);


        // Date
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        java.sql.Date dateSql = new java.sql.Date(date);

        System.out.print(dateSql);

        Commande commande = cs.creerCommande(utilisateurSaisi, articleSaisi);
        request.setAttribute("commande", commande);
        request.getRequestDispatcher("confirmation.jsp").include(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
