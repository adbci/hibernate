package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.UserService;
import fr.humanbooster.fx.cadeaux.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class ArticlesServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserService as = new UserServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		request.getRequestDispatcher("login.jsp").include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// on recupere les infos du formulaire d'inscription
		String nomUtilisateur = request.getParameter("email");
		String pwdUtilisateur = request.getParameter("pwd");

		Utilisateur utilisateur = as.loginUser(nomUtilisateur, pwdUtilisateur);

		System.out.println("Check utilisateur :"+utilisateur.toString());
		int idUserDB = utilisateur.getIdUtilisateur();

		System.out.println("Check id utilisateur :"+idUserDB);

		HttpSession session = request.getSession();


		// si l'user est connecté
		if(utilisateur != null) {

			System.out.println(" ---------------- USER CONNECTE -----------");
			System.out.println(utilisateur.toString());

			// on enrichit l'objet request avec l'objet Utilisateur
			request.getSession().setAttribute("utilisateur", utilisateur);
			request.getSession().setAttribute("idUtilisateur", utilisateur.getIdUtilisateur());

			request.setAttribute("utilisateur", utilisateur);
			request.setAttribute("idUtilisateur", utilisateur.getIdUtilisateur());

			// on confie l'objet request à la JSP utilisateurs.jsp qui est la VUE
			request.getRequestDispatcher("profile.jsp").include(request, response);
		} else {
			request.setAttribute("message", "Erreur connection");

			request.getRequestDispatcher("login.jsp").include(request, response);

		}




	}




}
