package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.UserService;
import fr.humanbooster.fx.cadeaux.service.VilleService;
import fr.humanbooster.fx.cadeaux.service.impl.UserServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.VilleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class ArticlesServlet
 */
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserService userService = new UserServiceImpl();
	private VilleService vs = new VilleServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InscriptionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Recupere toutes les utilisateurs
		List<Utilisateur> utilisateurs = userService.recupereUtilisateurs();
		request.setAttribute("utilisateurs", utilisateurs);

		// Recupere toutes les villes
		List<Ville> villes = vs.recupereVilles();
		request.setAttribute("villes", villes);

		// et les envoies la JSP
		request.getRequestDispatcher("utilisateurs.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// on recupere les infos du formulaire d'inscription
		String nomUtilisateur = request.getParameter("nom");
		String prenomUtilisateur = request.getParameter("prenom");
		String emailUtilisateur = request.getParameter("email");
		String pwdUtilisateur = request.getParameter("pwd");
		int villeUtilisateur = Integer.parseInt(request.getParameter("ville"));

		// on cherche l'objet ville dans la base depuis l'id fourni par le formulaire
		Ville villeSaisie = vs.recupereVilleById(villeUtilisateur);

		Utilisateur utilisateur = userService.createUser(nomUtilisateur, prenomUtilisateur, emailUtilisateur, pwdUtilisateur, villeSaisie);

		// on enrichit l'objet request avec la'objet Utilisateur
		request.setAttribute("utilisateur", utilisateur);

		HttpSession session = request.getSession();

		// si l'inscription s'est déroulé avec succès
		if(utilisateur!=null){
			request.setAttribute("inscrit", true);
		}
		// si l'user est connecté
		if(session.getAttribute("idUtilisateur") != null) {
			request.setAttribute("connecte", true);
		}


/*
        response.getWriter().append("<html><body>Vous avez saisi : ");
        response.getWriter().append(utilisateur.getNomUtilisateur() + " ");
        response.getWriter().append(prenomUtilisateur + " ");
        response.getWriter().append(emailUtilisateur + " ");
        response.getWriter().append("<a href='connexion.jsp'>Se connecter</a></body></html>");
 */

		System.out.println("Servlet inscription");
		System.out.println("Vous avez saisi : ");
		System.out.println(nomUtilisateur + " ");
		System.out.println(prenomUtilisateur + " ");
		System.out.println(emailUtilisateur + " ");


		// Recupere toutes les villes
		List<Ville> villes = vs.recupereVilles();
		request.setAttribute("villes", villes);

		// Recupere toutes les utilisateurs et les envoies la JSP
		List<Utilisateur> utilisateurs = userService.recupereUtilisateurs();
		request.setAttribute("utilisateurs", utilisateurs);

		// on confie l'objet request à la JSP utilisateurs.jsp qui est la VUE
		request.getRequestDispatcher("utilisateurs.jsp").include(request, response);

	}




}
