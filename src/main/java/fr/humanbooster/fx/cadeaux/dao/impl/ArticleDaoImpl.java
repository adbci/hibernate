package fr.humanbooster.fx.cadeaux.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;

public class ArticleDaoImpl implements ArticleDao {

	private Session session;
	private Transaction transaction;

	@Override
	public List<Article> findAll() {

		// on passe par la session hibernate pour récupérer
		// l'ensemble des articles
		// en paramètre de la méthode createQuery, on donne une requete HQL
		return session.createQuery("from Article").getResultList();
	}

	public Article createArticle(Article article){
		session.save(article);
		return article;
	}

	public Article updateArticle(Article article){
		session.saveOrUpdate(article);
		return article;
	}

	public boolean deleteIdArticle(int idArticle){
		Article article = this.findById(idArticle);
		if (article==null) return false;
		session.delete(idArticle);
		return false;
	}

	public Article findById(int idArticle) {
		return session.byId(Article.class).load(idArticle);
	}

	@Override
	public Session openCurrentSession() {
		session = getSessionFactory().openSession();
		return session;

	}

	@Override
	public Session openCurrentSessionWithTransaction() {
		session = getSessionFactory().openSession();
		transaction = session.beginTransaction();
		return session;
	}

	@Override
	public void closeCurrentSession() {
		session.close();
	}

	@Override
	public void closeCurrentSessionwithTransaction() {
		transaction.commit();
		session.close();

	}

	private SessionFactory getSessionFactory() {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml")
				.build();
		Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

		return sessionFactory;

	}

}
