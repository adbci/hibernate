package fr.humanbooster.fx.cadeaux.dao;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Ville;
import org.hibernate.Session;

import fr.humanbooster.fx.cadeaux.business.Article;

public interface ArticleDao {

	public List<Article> findAll();

	public Article findById(int idArticle);

	public Session openCurrentSession();

	public Session openCurrentSessionWithTransaction();

	public void closeCurrentSession();

	public void closeCurrentSessionwithTransaction();
}
