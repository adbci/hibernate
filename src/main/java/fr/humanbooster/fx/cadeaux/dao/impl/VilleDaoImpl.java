package fr.humanbooster.fx.cadeaux.dao.impl;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;
import fr.humanbooster.fx.cadeaux.dao.VilleDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class VilleDaoImpl implements VilleDao {

	private Session session;
	private Transaction transaction;

	@Override
	public List<Ville> findAll() {

		// on passe par la session hibernate pour récupérer
		// l'ensemble des articles
		// en paramètre de la méthode createQuery, on donne une requete HQL
		return session.createQuery("from Ville").getResultList();
	}

	public Ville findById(int idVille) {
		return session.byId(Ville.class).load(idVille);
	}

	@Override
	public Session openCurrentSession() {
		session = getSessionFactory().openSession();
		return session;

	}

	@Override
	public Session openCurrentSessionWithTransaction() {
		session = getSessionFactory().openSession();
		transaction = session.beginTransaction();
		return session;
	}

	@Override
	public void closeCurrentSession() {
		session.close();
	}

	@Override
	public void closeCurrentSessionwithTransaction() {
		transaction.commit();
		session.close();

	}

	private SessionFactory getSessionFactory() {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml")
				.build();
		Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

		return sessionFactory;

	}

}
