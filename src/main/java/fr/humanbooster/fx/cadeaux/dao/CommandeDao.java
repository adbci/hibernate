package fr.humanbooster.fx.cadeaux.dao;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import org.hibernate.Session;

import java.util.Date;
import java.util.List;

public interface CommandeDao {

	public List<Commande> findAll();

	public Commande creerCommande(Utilisateur utilisateur, Article article);

	public Session openCurrentSession();

	public Session openCurrentSessionWithTransaction();

	public void closeCurrentSession();

	public void closeCurrentSessionwithTransaction();
}
