package fr.humanbooster.fx.cadeaux.dao.impl;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.dao.UserDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class UserDaoImpl implements UserDao {

	private Session session;
	private Transaction transaction;

	@Override
	public List<Utilisateur> findAll() {

		// on passe par la session hibernate pour récupérer
		// l'ensemble des utilisateurs
		// en paramètre de la méthode createQuery, on donne une requete HQL
		return session.createQuery("from Utilisateur ").getResultList();
	}

	public Utilisateur loginUser(String email, String pwd){
		return (Utilisateur) session.createQuery("from Utilisateur WHERE email = '"+email+"' AND pwd='"+pwd+"'").getSingleResult();
	}

	public Utilisateur create(Utilisateur utilisateur){
		session.save(utilisateur);
		return utilisateur;
	}

	public Utilisateur update(Utilisateur utilisateur){
		session.saveOrUpdate(utilisateur);
		return utilisateur;
	}

	public Utilisateur delete(int idUtilisateur){
		Utilisateur utilisateur = this.findById(idUtilisateur);
		if (utilisateur==null) return utilisateur;
		session.delete(utilisateur);
		return utilisateur;
	}

	public Utilisateur findById(int idUtilisateur) {
		return session.byId(Utilisateur.class).load(idUtilisateur);
	}

	@Override
	public Session openCurrentSession() {
		session = getSessionFactory().openSession();
		return session;

	}

	@Override
	public Session openCurrentSessionWithTransaction() {
		session = getSessionFactory().openSession();
		transaction = session.beginTransaction();
		return session;
	}

	@Override
	public void closeCurrentSession() {
		session.close();
	}

	@Override
	public void closeCurrentSessionwithTransaction() {
		transaction.commit();
		session.close();

	}

	private SessionFactory getSessionFactory() {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml")
				.build();
		Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

		return sessionFactory;

	}

}
