package fr.humanbooster.fx.cadeaux.dao.impl;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.dao.ArticleDao;
import fr.humanbooster.fx.cadeaux.dao.CommandeDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

public class CommandeDaoImpl implements CommandeDao {

	private Session session;
	private Transaction transaction;

	@Override
	public List<Commande> findAll() {

		// on passe par la session hibernate pour récupérer
		// l'ensemble des commandes
		// en paramètre de la méthode createQuery, on donne une requete HQL
		return session.createQuery("from Commande").getResultList();
	}

	public Commande creerCommande(Utilisateur utilisateur, Article article){
		Commande commande = new Commande(utilisateur, article);
		session.save(commande);
		return commande;
	}


	public boolean deleteCommande(int idCommande){
		Commande commande = this.findById(idCommande);
		if (commande==null) return false;
		session.delete(idCommande);
		return false;
	}

	public Commande findById(int idCommande) {
		return session.byId(Commande.class).load(idCommande);
	}

	@Override
	public Session openCurrentSession() {
		session = getSessionFactory().openSession();
		return session;

	}

	@Override
	public Session openCurrentSessionWithTransaction() {
		session = getSessionFactory().openSession();
		transaction = session.beginTransaction();
		return session;
	}

	@Override
	public void closeCurrentSession() {
		session.close();
	}

	@Override
	public void closeCurrentSessionwithTransaction() {
		transaction.commit();
		session.close();

	}

	private SessionFactory getSessionFactory() {

		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml")
				.build();
		Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

		return sessionFactory;

	}

}
