package fr.humanbooster.fx.cadeaux.dao;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import org.hibernate.Session;

import java.util.List;

public interface UserDao {

	public List<Utilisateur> findAll();

	public Utilisateur create(Utilisateur utilisateur);

	public Utilisateur loginUser(String email, String pwd);

	public Utilisateur update(Utilisateur utilisateur);

	public Utilisateur delete(int idUtilisateur);

	public Utilisateur findById(int idUtilisateur);

	public Session openCurrentSession();

	public Session openCurrentSessionWithTransaction();

	public void closeCurrentSession();

	public void closeCurrentSessionwithTransaction();
}
