package fr.humanbooster.fx.cadeaux.dao;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Ville;
import org.hibernate.Session;

import java.util.List;

public interface VilleDao {

	public List<Ville> findAll();

	public Ville findById(int idVille);

	public Session openCurrentSession();

	public Session openCurrentSessionWithTransaction();

	public void closeCurrentSession();

	public void closeCurrentSessionwithTransaction();
}
