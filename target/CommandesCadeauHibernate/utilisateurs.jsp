<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Liste des utilisateurs</title>
</head>
<body>
<div class="row">
    <div class="col-md-offset-1 col-md-4">
        <h2>Utilisateurs</h2>
        <p>Nombre total d'utilisateurs: ${utilisateurs.size()}</p>
        <table>
            <tr>
                <td>nom</td>
                <td>prenom</td>
                <td>email</td>
                <td>pwd</td>
                <td>ville</td>
            </tr>
            <c:forEach var="utilisateurs" items="${utilisateurs}">
                <tr>
                    <td> ${utilisateurs.nom}</td>
                    <td> ${utilisateurs.prenom}</td>
                    <td> ${utilisateurs.email}</td>
                    <td> ${utilisateurs.pwd}</td>
                    <td> ${utilisateurs.ville.nomVille}</td>
                </tr>
            </c:forEach>
        </table>
    </div>


    <div class="col-md-offset-1 col-md-4">
       <h2>Ajouter un utilisateur</h2>


        <form action="/inscription" method="post">
            Nom: <input type="text" value="Dzz" name="nom"> <br/>
            Prenom: <input type="text" value="Jean" name="prenom"><br/>
            Email: <input type="text" value="jean@mail.org" name="email"><br/>
            Pwd: <input type="text" value="000000" name="pwd"><br/>

            <p>Nombre total de villes: ${villes.size()}</p>

            <select name="ville">
                <c:forEach items="${villes}" var="ville">
                    <option value="${ville.idVille}">${ville.nomVille}</option>
                </c:forEach>
            </select>
            <input type="submit" value="AJOUTER" class="btn">
            <br/>
            <a href='/login' class="btn">Se connecter</a>
        </form>
    </div>
</div>

</body>
</html>

</body>
</html>
