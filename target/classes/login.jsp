<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Page personnelle</title>
</head>
<body>

<br/>
<br/>
<br/>
<br/>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <c:forEach var="message" items="${message}">
            ${message}
        </c:forEach>
    </div>
</div>
<div class="row">
    <form action="/login" method="post">

        <div class="col-md-offset-4 col-md-4 text-center">

            Email: <input type="text" value="ad@test.com" name="email"><br/>
            Pwd: <input type="text" value="45654" name="pwd"><br/>
        </div>
        <div><br/> </div>
        <div class="col-md-offset-3 col-md-6 text-center">
            <input type="submit" value="LOGIN" class="btn">
            <a href='/login' class="btn">Se connecter</a>
        </div>
    </form>
</div>

</body>
</html>

</body>
</html>
