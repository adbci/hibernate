<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste des Articles</title>
</head>
<body>
	<h1>Catalogue de cadeaux</h1>
	<br>
	<table>
		<form action="/commander" method="post">
		<tr>
			<td>Designation</td>
			<td>Description</td>
			<td>Categorie</td>
			<td>Prix</td>
			<td>Stock</td>
			<td>Commander</td>

		</tr>
	<c:forEach var="article" items="${articles}">
		<tr>
			<td>${article.designation}</td>
			<td>${article.description}</td>
			<td>${article.categorie.nom}</td>
			<td>${article.nbPoints}</td>
			<td>${article.stock}</td>
			<td><a href="/acheter?id=${article.idArticle}">ACHETER</a> </td>
		</tr>
	</c:forEach>
		</form>
	</table>
	<p>Nombre total d'articles dans le catalogue: ${articles.size()}</p>
</body>
</html>
