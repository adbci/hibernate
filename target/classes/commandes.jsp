<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Liste des utilisateurs</title>
</head>
<body>
<div class="row">
    <div class="col-md-offset-1 col-md-4">
        <h2>Commandes</h2>

        <c:forEach var="commande" items="${commandes}">
            <tr>
                <td> ${commande.idCommande}</td>
                <td> ${commande.dateCommande}</td>

                <td> ${commande.article.designation}</td>
                <td> ${commande.utilisateur.prenom} ${commande.utilisateur.nom}</td>
            </tr>
        </c:forEach>


    </div>

    <div class="col-md-offset-1 col-md-4">
        <h2>Passer une commande</h2>
        <a href='/index' class="btn">Passer une commande</a>
    </div>
</div>

</body>
</html>

</body>
</html>
